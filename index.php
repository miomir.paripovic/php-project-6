<!DOCTYPE html>
<html>

<head>
    <title>Project no. 6</title>
    <!-- External CSS file -->
    <link rel="stylesheet" type="text/css" href="css/style.css">    
    <!-- Icon for tab -->
    <link rel="icon" href="images/favicon.png">
</head>

<body class="body">
    <div class="wrapper">
        <header class="header">
            <img  class="header__logo-image" src="images/phplogo.png" alt="Logo image" title="Logo image">
        </header>        
        <!-- End of header -->
        <div class="main"> 
            <!-- Left side panel -->
            <div class="left-panel">
                <?php
                include 'input/input.php';                
                echo '<span class="dump">';
                var_dump($arrayElement);
                var_dump($mixedArray);
                echo '</span>';
                ?>
            </div>
            <!-- Mid side panel -->
            <div class="middle-panel">
                <?php
                for ($x = 0; $x < count($mixedArray); $x++) {
                    if (is_int($mixedArray[$x])) {
                        echo 'The element at position ' . $x . ' ';
                        echo 'is an integer: ' . '<span class="integer">' . $mixedArray[$x] . '</span><br>';
                    }
                    elseif (is_float($mixedArray[$x])) {
                        echo 'The element at position ' . $x . ' ';
                        echo 'is a float (double): ' . '<span class="float">' . $mixedArray[$x] . '</span><br>';
                    }
                    elseif (is_string($mixedArray[$x])) {
                        echo 'The element at position ' . $x . ' ';
                        echo 'a string: ' . '<span class="string">' . $mixedArray[$x] . '</span><br>';
                    }
                    elseif (is_bool($mixedArray[$x])) {
                        echo 'The element at position ' . $x . ' ';
                        echo 'a boolean: ' . '<span class="bool">';
                        /* echo prints true as 1, but not 0 for false
                           this one-liner (ternary operator) is an elegant solution;
                           the expression (expr1) ? (expr2) : (expr3) evaluates to expr2
                           if expr1 evaluates to TRUE, and expr3 if expr1 evaluates to FALSE */
                           
                        echo true == $mixedArray[$x] ? 'true' : 'false';
                        echo '</span><br>';                        
                    }
                    elseif (is_array($mixedArray[$x])) {
                        $newArray = $mixedArray[$x];                        
                        echo 'The element at position ' . $x . ' ';
                        echo 'is an array with<br>';
                        echo 'elements: ';
                        echo '<span class="array">';
                        for ($i = 0; $i < count($newArray); $i++) {
                            echo $newArray[$i] . ' ';
                        }
                        echo '</span><br>';                        
                    }
                }    
                ?>                
            </div>
            <!-- Right side panel -->
            <div class="right-panel">                
                <form action="index.php" method="post">     
                    <label class="label" for="color">Choose a valid CSS color (default is 'maroon')</label><br>
                    <input class="input" type="text" name="color" placeholder="Color"><br>
                    <label class="label" for="stringarray">Enter space separated numbers (default is null)</label><br>
                    <textarea class="textarea" rows="4" cols="50" name="stringarray" placeholder="Enter numbers here... (e.g. -1 2 1.5 3.14)" ></textarea><br>
                    <p class="info">Pressing 'Process' button with blank fields will set values to default: 'maroon' color and empty array.</p>
                    <input type="submit" value="Process">                    
                </form>
                <!-- form ends-->
                <?php                
                include 'lib/functions.php';               
                
                if (empty($_POST['color'])) { 
                    $color = 'maroon';
                }
                else {
                    $color = $_POST['color'];
                }
                
                if (empty($_POST['stringarray'])) {
                    $setOfNumbers = null;                
                }
                else {
                    $stringArray = $_POST['stringarray'];
                    // space is separator instead comma
                    $setOfNumbers = array_map("returnFloat", str_getcsv($stringArray, " "));
                }
                
                echo '<br>The color is: ';                
                echo '<span class="color" style="color:' . $color . '">'. $color . '</span><br>';
                echo 'The elements of the array are:<br>';
                
                if ($setOfNumbers == null) {
                    echo '-none-';
                    //var_dump($setOfNumbers);
                }
                else {                    
                    for ($i = 0; $i < count($setOfNumbers); $i++) {                    
                        if ($i != count($setOfNumbers) - 1) {
                            echo $setOfNumbers[$i] . ', ';
                        }
                        else {
                            echo $setOfNumbers[$i];
                        }                    
                    }
                }
                echo '<br><br>';                
                echo 'The sum is: ';
                // include external file with function definition                
                echo colorSum($color, $setOfNumbers);
                echo '<br>';                
                echo 'The min is: ';
                echo colorMin($color, $setOfNumbers);
                echo '<br>';    
                echo 'The max is: ';
                echo colorMax($color, $setOfNumbers);
                ?>
            </div>
        </div>        
    </div>
</body>

</html>