<?php
function colorSum($color, $someSet) {
        $sum = 0;
        for ($i = 0; $i < count($someSet); $i++) {
            $sum += $someSet[$i];            
        }        
        return '<span style="color:' . $color . '">' . (string)$sum . '</span><br>';
}

function colorMin($color, $someSet) {
    $min = $someSet[0];
    for ($i = 0; $i < count($someSet); $i++) {
        if ($someSet[$i] < $min) {
            $min = $someSet[$i];
        }            
    }
    return '<span style="color:' . $color . '">' . (string)$min . '</span><br>';
}

function colorMax($color, $someSet) {
    $max = $someSet[0];
    for ($i = 0; $i < count($someSet); $i++) {
        if ($someSet[$i] > $max) {
            $max = $someSet[$i];
        }            
    }
    return '<span style="color:' . $color . '">' . (string)$max . '</span><br>';
}

function returnFloat ($value) {
    return (float)$value;
}
?>