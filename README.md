# Project no. 6

## Task

This is my assignment for week no. 6. The main goal for the project is to create a web page (index.php) which contains variables of int, string, float (double), bool and array type. Print variables to the output using different colors - one color for integer values, one for float, etc. Then define a function which accepts two parameters: color name (of the string type, like red, blue, etc.) and an array (number elements). Function should sum all the elements of the array and present the result in color which was set with first parameter. Function definition should be in external file (funtions.php) which should be included to the main file (index.php).

## Update: Saturday, March 17

Two functions are added to functions.php. The first one is colorMin, which finds the lowest number in the provided array and prints that value in desired color. The second one is colorMax, which prints the maximum number of the array in desired color.

## Update: Sunday, March 18

Added: possibility to provide a color and an array through the input fields.